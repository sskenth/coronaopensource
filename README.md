# README #
--------Pokemon Go----------------

In less than 100 lines of code you can create the capture a pokemon part of PokemonGo, see video here - https://www.youtube.com/watch?v=SRVgbjwYeGo



--------Camera Follow trail-----
This small project is designed as a learning tool. So feel free to use it. Please notes its my first open source project so its project not perfect, drop me a line at sskenth (@) gmail (.) com if you have any advice or would like this fleshed out some more.

* Simple code demonstrating camera trail/follow using sprite animation tutorial from CoronaSDK and camera perspective code from Caleb P (Gymbyl Coding) - https://gist.github.com/GymbylCoding/8675733

Video demonstration here - https://www.youtube.com/watch?v=XMRo-DqyMOc

contains a few folders breaking down how I got to the final camera follow code.


Take a look at the latest game I'm making here - https://ninjamangagame.wordpress.com/